#ifndef OROCOS_DIAGUI_USG_TELEMANIPULATION_COMPONENT_HPP
#define OROCOS_DIAGUI_USG_TELEMANIPULATION_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include "satler_filtered_grt_rtw/satler_filtered.h"
#include "ddm_api.h"

#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>

#include <WRUT/TelemanipulationData.h>
#include <remedi_usg_ros_udp/Teleop_Feedback.h>
//#include <std_msgs/Float64MultiArray.h>

#include <percro_6dofhaptic/Status.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/TwistStamped.h>

class Diagui_usg_telemanipulation :
		public RTT::TaskContext,
		public satler_filteredModelClass{

public:

	//	pose_pub_     = ros_node_->advertise<geometry_msgs::PoseStamped>("endeffector_position", 10);
	//		velocity_pub_ = ros_node_->advertise<geometry_msgs::TwistStamped>("endeffector_velocity", 10);
	//		force_pub_    = ros_node_->advertise<geometry_msgs::WrenchStamped>("endeffector_force", 10);
	//		status_pub_   = ros_node_->advertise<percro_6dofhaptic::Status>("endeffector_status", 10);

	RTT::InputPort<remedi_usg_ros_udp::Teleop_Feedback> InputWavePortFromRobot; 			//data containing 3 wave variables
	RTT::InputPort<geometry_msgs::PoseStamped> DeltaPositionPort; // velocity form delta
	RTT::InputPort<geometry_msgs::TwistStamped> DeltaVelocityPort; // velocity form delta
	RTT::InputPort<std_msgs::UInt8> Telemanipulation_Status_in; //input from DDM
	RTT::InputPort<percro_6dofhaptic::Status> Delta_Status_in; //input from DDM

	RTT::OutputPort<WRUT::TelemanipulationData> OutputWavePortToRobot; //data containig 3 wave variables
	RTT::OutputPort<geometry_msgs::WrenchStamped> ToDeltaForcePort; //data containing 3 forces
	RTT::OutputPort<std_msgs::UInt32> Telemanipulation_Status_out;  //output to DDM

	RTT::OutputPort<geometry_msgs::TwistStamped> DecodedVelocityPort; // velocity form delta
	RTT::OutputPort<geometry_msgs::WrenchStamped> DecodedForcePort; //data containing 3 forces



	//RTT::OutputPort<std::vector<double> > Control;

	// inputs for wave coding module
	double Vm_cmd; //master`s demanded velocity
	double Wv_rcv; //received slave demanded velocity

	// outputs of a wave coding module
	double Wu_cmd; //control to be send from master TODO FILTERING
	double Fs_rcv; //received force from the slave

	int actual_state;

	//wave impedancy is in matlab  model

	Diagui_usg_telemanipulation(std::string const& name);
	bool configureHook();
	bool startHook();
	void updateHook();
	void stopHook();
	void cleanupHook();

	void printOutData();
	void assignmentOfInputsWave(remedi_usg_ros_udp::Teleop_Feedback, geometry_msgs::TwistStamped);
	void setOutputValuesAndSend(geometry_msgs::PoseStamped, geometry_msgs::WrenchStamped);

	void setb(float , int );
	void getb(int);

	void waveCoding(void );

	void setInputWaveDataAvailable(void ){
		InputWaveDataAvailable = true;
	}

	void setDeltaVelocityDataAvailable(void ){
		DeltaVelocityDataAvailable = true;
	}

	void clearInputWaveDataAvailable(void ){
		InputWaveDataAvailable = false;
	}

	void clearDeltaVelocityDataAvailable(void ){
		DeltaVelocityDataAvailable = false;
	}

	bool isInputWaveDataAvailable(void ){
		return InputWaveDataAvailable;
	}

	bool isDeltaVelocityDataAvailable(void ){
		return DeltaVelocityDataAvailable;
	}

private:
	double b;
	bool InputWaveDataAvailable;
	bool DeltaVelocityDataAvailable;
};
#endif
