#include "diagui_usg_telemanipulation-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

Diagui_usg_telemanipulation::Diagui_usg_telemanipulation(std::string const& name) : TaskContext(name){

	this->ports()->addPort( "InputWavePortFromRobot", InputWavePortFromRobot )
													.doc("Output port for InputWavePortFromRobot" );

	this->ports()->addPort( "DeltaPositionPort", DeltaPositionPort )
													.doc("Output port for DeltaPositionPort" );

	this->ports()->addPort( "DeltaVelocityPort", DeltaVelocityPort )
													.doc("Output port for DeltaVelocityPort" );

	this->ports()->addPort( "OutputWavePortToRobot", OutputWavePortToRobot )
													.doc("Output port for OutputWavePortToRobot" );

	this->ports()->addPort( "Delta_Status_in", Delta_Status_in )
													.doc("Input port for Delta_Status_in" );

	this->ports()->addPort( "ToDeltaForcePort", ToDeltaForcePort )
													.doc("Output port for DeltaVelocityPort" );

	this->ports()->addPort( "Telemanipulation_Status_out", Telemanipulation_Status_out )
													.doc("Output port for DeltaVelocityPort" );

	this->ports()->addPort( "DecodedForcePort", DecodedForcePort )
													.doc("Output port for DecodedForcePort" );

	this->ports()->addPort( "DecodedVelocityPort", DecodedVelocityPort )
													.doc("Output port for DecodedVelocityPort" );


	this->addOperation( "setb", &Diagui_usg_telemanipulation::setb,
			this, RTT::OwnThread).doc("reset");

	this->addOperation( "getb", &Diagui_usg_telemanipulation::getb,
			this, RTT::OwnThread).doc("reget");

	b=10;
	InputWaveDataAvailable = false;
	DeltaVelocityDataAvailable = false;


	//	this->ports()->addEventPort( "State", State )
	//		    								.doc("Input port for platform state" );
	std::cout << "Diagui_usg_telemanipulation constructed !" <<std::endl;

}

bool Diagui_usg_telemanipulation::configureHook(){
	std::cout << "Diagui_usg_telemanipulation configured !" <<std::endl;
	return true;
}

bool Diagui_usg_telemanipulation::startHook(){
	std::cout << "Diagui_usg_telemanipulation started !" <<std::endl;
	actual_state = 0;
	initialize();

	return true;
}

void Diagui_usg_telemanipulation::updateHook(){
	//
	//	1. Read from the InputWave port
	//	2. Read from haptic intreface port
	//	3. Compute values
	//	4. Write and send via OutputWave port

  //	std::cout << "Diagui_usg_telemanipulation updated !" <<std::endl;

	remedi_usg_ros_udp::Teleop_Feedback InputWaveDataFromRobot;
	geometry_msgs::PoseStamped DeltaPositionData;
	geometry_msgs::TwistStamped DeltaVelocityData;
	std_msgs::UInt8 DDMStatusData;

	WRUT::TelemanipulationData OutputWaveDataToRobot;
	geometry_msgs::WrenchStamped toDeltaForceData;
	std_msgs::UInt8 TelemanipulationStatusData;


	actual_state=DDM_CODE_STANDBY;
	//std::cout << "\033[2J\033[1;1H";

	//std::cout << "Przed \n";

	if(InputWavePortFromRobot.read(InputWaveDataFromRobot) == RTT::NewData){
		setInputWaveDataAvailable();
		std::cout << "Input Wave available \n";
	}

	//std::cout << "Po \n";

	if(DeltaVelocityPort.read(DeltaVelocityData) == RTT::NewData){
		setDeltaVelocityDataAvailable();
				std::cout << "Delta velocity available \n";
	}

	if (isInputWaveDataAvailable() && isDeltaVelocityDataAvailable()){


		std::cout << "************************************** " <<  std::endl;
		actual_state = DDM_CODE_READY;
		DeltaPositionPort.read(DeltaPositionData);

		// computing wave variable form
		assignmentOfInputsWave(InputWaveDataFromRobot, DeltaVelocityData);
		step();
		printOutData();

		// we have to send delta position and orientation, force to delta and the wave to robot
		setOutputValuesAndSend(DeltaPositionData,InputWaveDataFromRobot.Wrench);

		clearInputWaveDataAvailable();
		clearDeltaVelocityDataAvailable();

		percro_6dofhaptic::Status Delta_Status;
		//		std::cout << "AAA \n";
		if(Delta_Status_in.read( Delta_Status) == RTT::NewData){
			std_msgs::UInt32 tmp;
			//	std::cout << "AAA " << Delta_Status.button_press << std::endl ;
			if (Delta_Status.button_press == 60){
				tmp.data=4294967295;
				std::cout << "Delta pressed \n";
			}
			else	
				tmp.data=0;

			Telemanipulation_Status_out.write(tmp);
							
			
		}
		setInputWaveDataAvailable();
		//clearInputWaveDataAvailable();
		
		std::cout << "************************************** " <<  std::endl;
	}

	//	std::cout << "Diagui_usg_telemanipulation executes updateHook !" <<std::endl;
}


void Diagui_usg_telemanipulation::printOutData(void ){

  std::cout << "Inputs to the blocks \n " << satler_filtered_U.Vm_cmdX << std::endl;
  std::cout << "Speed coming from master x: " << satler_filtered_U.Vm_cmdX << std::endl;
  std::cout << "Speed coming from master y: " << satler_filtered_U.Vm_cmdY << std::endl;
  std::cout << "Speed coming from master z: " << satler_filtered_U.Vm_cmdZ << std::endl;

  std::cout << "Wave Coming from robot x: " << satler_filtered_U.Wv_rcvX << std::endl;
  std::cout << "Wave Coming from robot y: " << satler_filtered_U.Wv_rcvY << std::endl;
  std::cout << "Wave Coming from robot z: " << satler_filtered_U.Wv_rcvZ << std::endl;

  std::cout << "Outputs from the block \n " << satler_filtered_U.Vm_cmdX << std::endl;
  std::cout << "Wave going to robot x: " << satler_filtered_Y.Wu_cmdX << std::endl;
  std::cout << "Wave going to robot y: " << satler_filtered_Y.Wu_cmdY << std::endl;
  std::cout << "Wave going to robot z: " << satler_filtered_Y.Wu_cmdZ << std::endl;

  std::cout << "Force from slave x: " << satler_filtered_Y.Fs_rcvX << std::endl;
  std::cout << "Force from slave y: " << satler_filtered_Y.Fs_rcvY << std::endl;
  std::cout << "Force from slave z: " << satler_filtered_Y.Fs_rcvZ << std::endl;



}

void Diagui_usg_telemanipulation::setOutputValuesAndSend(
		geometry_msgs::PoseStamped positionFromDelta,
		geometry_msgs::WrenchStamped ForceFromRobot){

	// we have to send delta position and orientation, force to delta and the wave to robot
	//setOutputValuesAndSend(DeltaPositionData,toDeltaForceData);

	WRUT::TelemanipulationData waveDataToBeSendAndPosition;
	geometry_msgs::WrenchStamped forceDataToBeSend;

	geometry_msgs::TwistStamped decodedVelocityToBeSend;
	geometry_msgs::WrenchStamped decodedForceToBeSend;

	waveDataToBeSendAndPosition.waveVariable[0] = satler_filtered_Y.Wu_cmdX;
	waveDataToBeSendAndPosition.waveVariable[1] = satler_filtered_Y.Wu_cmdY;
	waveDataToBeSendAndPosition.waveVariable[2] = satler_filtered_Y.Wu_cmdZ;

	waveDataToBeSendAndPosition.endEffectorPosition[0] = positionFromDelta.pose.position.x;
	waveDataToBeSendAndPosition.endEffectorPosition[1] = positionFromDelta.pose.position.y;
	waveDataToBeSendAndPosition.endEffectorPosition[2] = positionFromDelta.pose.position.z;

	waveDataToBeSendAndPosition.endEffectorOrientation[0] = -positionFromDelta.pose.orientation.z;
	waveDataToBeSendAndPosition.endEffectorOrientation[1] = positionFromDelta.pose.orientation.y;
	waveDataToBeSendAndPosition.endEffectorOrientation[2] = -positionFromDelta.pose.orientation.x;


	OutputWavePortToRobot.write(waveDataToBeSendAndPosition);


	  std::cout << "ForceFromRobot.wrench.force.x: " << ForceFromRobot.wrench.force.x << std::endl;
	  std::cout << "ForceFromRobot.wrench.force.y: " << ForceFromRobot.wrench.force.y << std::endl;
	  std::cout << "ForceFromRobot.wrench.force.z: " << ForceFromRobot.wrench.force.z << std::endl;


	//forceDataToBeSend.wrench.force.x = ForceFromRobot.wrench.force.x;
	//forceDataToBeSend.wrench.force.y = ForceFromRobot.wrench.force.y;
    //forceDataToBeSend.wrench.force.z = ForceFromRobot.wrench.force.z;

	forceDataToBeSend.wrench.force.x = satler_filtered_Y.Fs_rcvX;
	forceDataToBeSend.wrench.force.y = satler_filtered_Y.Fs_rcvY;
	forceDataToBeSend.wrench.force.z = satler_filtered_Y.Fs_rcvZ;

	ToDeltaForcePort.write(forceDataToBeSend);


	decodedForceToBeSend.wrench.force.x = satler_filtered_Y.Fs_rcvX;
	decodedForceToBeSend.wrench.force.y = satler_filtered_Y.Fs_rcvY;
	decodedForceToBeSend.wrench.force.z = satler_filtered_Y.Fs_rcvZ;

	DecodedForcePort.write(decodedForceToBeSend);
	std::cout << "Success " << decodedForceToBeSend.wrench.force.x <<"\n";
	


}

void Diagui_usg_telemanipulation::assignmentOfInputsWave(
		remedi_usg_ros_udp::Teleop_Feedback Wave,
		geometry_msgs::TwistStamped Velocities ){
	
	if (std::abs(Velocities.twist.linear.x) < 0.01)
		Velocities.twist.linear.x=0;

	if (std::abs(Velocities.twist.linear.y) < 0.01)
		Velocities.twist.linear.y=0;
	
	if (std::abs(Velocities.twist.linear.z) < 0.01)
		Velocities.twist.linear.z=0;

  satler_filtered_U.Vm_cmdX = Velocities.twist.linear.x;
  satler_filtered_U.Vm_cmdY = Velocities.twist.linear.y;
  satler_filtered_U.Vm_cmdZ = Velocities.twist.linear.z;

  // satler_filtered_U.Vm_cmdX = 0;
  // satler_filtered_U.Vm_cmdY = 0;
  // satler_filtered_U.Vm_cmdZ = 0;


	/*
	satler_filtered_U.Vm_cmdX = Velocities.twist.angular.x;
	satler_filtered_U.Vm_cmdY = Velocities.twist.angular.y;
	satler_filtered_U.Vm_cmdZ = Velocities.twist.angular.z;
	*/

	satler_filtered_U.Wv_rcvX = Wave.Wave_var_OUT[0];
	satler_filtered_U.Wv_rcvY = Wave.Wave_var_OUT[1];
	satler_filtered_U.Wv_rcvZ = Wave.Wave_var_OUT[2];

	std::cout << " Wave.Wave_var_OUT[0]: " << Wave.Wave_var_OUT[0] << std::endl;
	std::cout << " Wave.Wave_var_OUT[1]: " << Wave.Wave_var_OUT[1] << std::endl;
	std::cout << " Wave.Wave_var_OUT[2]: " << Wave.Wave_var_OUT[2] << std::endl;


}

void Diagui_usg_telemanipulation::stopHook() {
	std::cout << "Diagui_usg_telemanipulation executes stopping !" <<std::endl;
	actual_state=DDM_CODE_SHUTDOWN;
}

void Diagui_usg_telemanipulation::cleanupHook() {
	std::cout << "Diagui_usg_telemanipulation cleaning up !" <<std::endl;
}

void Diagui_usg_telemanipulation::waveCoding(void ){
	Fs_rcv = b * Vm_cmd - sqrt(2*b) * Wv_rcv;
	Wu_cmd = -Wv_rcv + sqrt(2*b) * Vm_cmd;
}

void Diagui_usg_telemanipulation::setb(float noweb, int no){

	switch(no)
	{
	case 0:
		satler_filtered_P.bX = noweb;
		break;

	case 1:
		satler_filtered_P.bY = noweb;
		break;

		//...
	case 2:
		satler_filtered_P.bZ = noweb;
		break;
	}

	getb(no);
}

void Diagui_usg_telemanipulation::getb(int no){
	std::cout << "Value of b ";
	switch(no)
	{
	case 0:
		std::cout << satler_filtered_P.bX << std::endl;
		break;

	case 1:
		std::cout << satler_filtered_P.bY << std::endl;
		break;

		//...
	case 2:
		std::cout << satler_filtered_P.bZ << std::endl;
		break;

	default:
		std::cout << "No such a number!" << std::endl;
		break;
	}

}
/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Diagui_usg_telemanipulation)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Diagui_usg_telemanipulation)
